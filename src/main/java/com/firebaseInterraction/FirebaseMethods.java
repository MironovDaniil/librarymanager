package com.firebaseInterraction;

import com.google.api.core.ApiFuture;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.cloud.firestore.WriteResult;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.FirestoreClient;
import com.library.Book;
import com.library.BookTemplate;
import com.library.Library;
import com.library.Stats;
import com.library.Student;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import static java.lang.Math.toIntExact;


public class FirebaseMethods {

    @Nullable
    public static PreBook unpackMapedBook(@NotNull Map<String, Object> packedBook) throws ParseException {   //Возвращает как ключ книгу, как значение id владельца, null если книга не на руках
        if (
                (!packedBook.containsKey("id"))
                        || (!packedBook.containsKey("pageCount"))
                        || (!packedBook.containsKey("grade"))
                        || (!packedBook.containsKey("publishYear"))
                        || (!packedBook.containsKey("title"))
                        || (!packedBook.containsKey("authorName"))
                        || (!packedBook.containsKey("publishingHouse"))
                        || (!packedBook.containsKey("timeGiven"))
                        || (!packedBook.containsKey("subject"))
                        || (!packedBook.containsKey("currentHandler"))) {
            return null;
        }
        UUID id = UUID.fromString((String) packedBook.get("id"));
        UUID currentHandler;
        if (packedBook.get("currentHandler") == null) {
            currentHandler = null;
        } else {
            currentHandler = UUID.fromString((String) packedBook.get("currentHandler"));
        }
        int publishYear = toIntExact((Long) packedBook.get("publishYear"));
        int grade = toIntExact((Long) packedBook.get("grade"));
        int pageCount = toIntExact((Long) packedBook.get("pageCount"));
        String title = (String) packedBook.get("title");
        String authorName = (String) packedBook.get("authorName");
        String publishingHouse = (String) packedBook.get("publishingHouse");
        String subject = (String) packedBook.get("subject");
        Date timeGiven = Library.DATE_FORMAT.parse((String) packedBook.get("timeGiven"));
        Book book = new Book(
                id,
                grade,
                pageCount,
                publishYear,
                title,
                authorName,
                publishingHouse,
                subject,
                null,
                timeGiven,
                null);
        return new PreBook(book, currentHandler);
    }

    @Nullable
    public static PreStudent unpackMapedStudent(@NotNull Map<String, Object> packedStudent) {
        if (
                (!packedStudent.containsKey("id"))
                        || (!packedStudent.containsKey("name"))
                        || (!packedStudent.containsKey("currentBooksOnHands"))
                        || (!packedStudent.containsKey("grade"))) {
            return null;
        }
        UUID id = UUID.fromString((String) packedStudent.get("id"));
        int grade = toIntExact((Long) packedStudent.get("grade"));
        String name = (String) packedStudent.get("name");
        Student student = new Student(id, grade, name, new ArrayList<>());
        PreStudent result;
        if (packedStudent.get("currentBooksOnHands") == null) {
            result = new PreStudent(student, new ArrayList<>());
        } else {
            List<UUID> booksId = new ArrayList<>();
            ArrayList<String> booksIdString = (ArrayList<String>) packedStudent.get("currentBooksOnHands");
            for (String s : booksIdString
            ) {
                booksId.add(UUID.fromString(s));
            }
            result = new PreStudent(student, booksId);
        }
        return result;
    }

    @Contract("null -> null")
    @Nullable
    public static Stats unpackMapedStats(Map<String, Object> packedStats) {
        if (packedStats == null) {
            return null;
        }
        if (
                (!packedStats.containsKey("studentsCount"))
                        || (!packedStats.containsKey("studentsPerGradeCount"))
                        || (!packedStats.containsKey("bookCount"))
                        || (!packedStats.containsKey("textbookPerGradeCount"))
                        || (!packedStats.containsKey("textbookPercentagePerGrade"))) {
            return null;
        }
        int studentsCount = toIntExact((Long) packedStats.get("studentsCount"));
        int[] studentsPerGradeCount = ((ArrayList<Long>) packedStats.get("studentsPerGradeCount"))
                .stream().mapToInt(Long::intValue).toArray();
        int bookCount = toIntExact((Long) packedStats.get("bookCount"));
        int[] textbookPerGradeCount = ((ArrayList<Long>) packedStats.get("textbookPerGradeCount"))
                .stream().mapToInt(Long::intValue).toArray();
        double[] textbookPercentagePerGrade = ((ArrayList<Double>) packedStats.get("textbookPercentagePerGrade"))
                .stream().mapToDouble(Double::doubleValue).toArray();
        return new Stats(studentsCount,
                studentsPerGradeCount,
                bookCount,
                textbookPerGradeCount,
                null,
                textbookPercentagePerGrade);
    }

    @Nullable
    public static BookTemplate unpackMapedTemplate(@NotNull Map<String, Object> packedTemplate) {
        if (
                (!packedTemplate.containsKey("title"))
                        || (!packedTemplate.containsKey("authorName"))
                        || (!packedTemplate.containsKey("grade"))
                        || (!packedTemplate.containsKey("subject"))) {
            return null;
        }
        int grade = toIntExact((Long) packedTemplate.get("grade"));
        String title = (String) packedTemplate.get("title");
        String authorName = (String) packedTemplate.get("authorName");
        String subject = (String) packedTemplate.get("subject");
        return new BookTemplate(grade, title, authorName, subject);
    }

    public static Firestore setUpFirebase() throws IOException {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        InputStream serviceAccount =
                loader.getResourceAsStream("librarymanager-sch291-firebase-adminsdk-vn56p-eb4448dba5.json");

        FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                .setDatabaseUrl("https://librarymanager-sch291.firebaseio.com")
                .build();

        FirebaseApp.initializeApp(options);
        return FirestoreClient.getFirestore();
    }

    public static void uploadBook(String libraryName, @NotNull Firestore dataBase, @NotNull Book book) throws ExecutionException, InterruptedException {
        DocumentReference docRef =
                dataBase.collection(libraryName + " books")
                        .document("Book " + book.getId().toString());
        ApiFuture<WriteResult> result = docRef.set(book.packToMap());
        System.out.println("Update time : " + result.get().getUpdateTime());
    }

    public static void uploadStudent(String libraryName, @NotNull Firestore dataBase, @NotNull Student student) throws ExecutionException, InterruptedException {
        DocumentReference docRef =
                dataBase.collection(libraryName + " students")
                        .document("Student " + student.getId().toString());
        ApiFuture<WriteResult> result = docRef.set(student.packToMap());
        System.out.println("Update time : " + result.get().getUpdateTime());
    }

    public static void uploadCompectStandard(Library library, @NotNull Firestore dataBase, List<BookTemplate>[] textbookComplectsPerGrade) throws ExecutionException, InterruptedException {
        DocumentReference docRef;
        ApiFuture<WriteResult> result;
        List<Map<String, Object>> added;
        Map<String, Object> addedDoc;
        for (int i = 0; i < 11; i++) {
            docRef = dataBase.collection(library.getName() + " standard")
                    .document("Standard of " + (i + 1) + " grade");
            added = new ArrayList<>();
            addedDoc = new HashMap<>(1);
            for (BookTemplate template : textbookComplectsPerGrade[i]
            ) {
                if (!library.getGroupsPerTemplate().containsKey(template)) {
                    System.out.println("No such book in library, can not add it to standard!");
                }
                added.add(template.packToMap());
            }
            addedDoc.put("Grade", (i + 1));
            addedDoc.put("Textbooks", added);
            result = docRef.set(addedDoc);
            System.out.println("Update time : " + result.get().getUpdateTime());
        }
    }

    public static void uploadNewBookToStandart(Library library, @NotNull Firestore dataBase, @NotNull Book book) throws Exception {
        if (book.getGrade() == 0) {
            throw new Exception("Can't add not a textbook to standart!");
        }
        BookTemplate bookTemplate = new BookTemplate(book);
        DocumentReference docRef;
        ApiFuture<WriteResult> result;
        List<Map<String, Object>> added;
        Map<String, Object> addedDoc;
        docRef = dataBase.collection(library.getName() + " standard")
                .document("Standard of " + book.getGrade() + " grade");
        ApiFuture<DocumentSnapshot> documentSnapshotApiFuture = dataBase.collection(library.getName() + " standard")
                .document("Standard of " + book.getGrade() + " grade").get();

        DocumentSnapshot documentSnapshot = documentSnapshotApiFuture.get();

        addedDoc = documentSnapshot.getData();
        added = (List<Map<String, Object>>) addedDoc.get("Textbooks");
        added.add(bookTemplate.packToMap());
        addedDoc = new HashMap<>(1);
        addedDoc.put("Grade", book.getGrade());
        addedDoc.put("Textbooks", added);
        result = docRef.set(addedDoc);
        System.out.println("Update time : " + result.get().getUpdateTime());
    }

    public static void uploadStats(String libraryName, @NotNull Firestore dataBase, @NotNull Stats stats) throws ExecutionException, InterruptedException {
        DocumentReference docRef =
                dataBase.collection(libraryName + " stats")
                        .document("Stats on " + Library.DATE_FORMAT.format(new Date()));
        ApiFuture<WriteResult> result = docRef.set(stats.packToMap());
        System.out.println("Update time : " + result.get().getUpdateTime());
        docRef =
                dataBase.collection(libraryName + " stats")
                        .document("Current stats");
        result = docRef.set(stats.packToMap());
        System.out.println("Update time : " + result.get().getUpdateTime());
    }

    public static void uploadLibrary(@NotNull Library library, Firestore dataBase) throws ExecutionException, InterruptedException {
        for (Map.Entry<UUID, Book> e : library.getBooksPerId().entrySet()) {
            Book book = e.getValue();
            uploadBook(library.getName(), dataBase, book);
        }
        for (Map.Entry<UUID, Student> e : library.getStudents().entrySet()) {
            Student student = e.getValue();
            uploadStudent(library.getName(), dataBase, student);
        }
    }

    public static void downloadLibrary(@NotNull Library library, @NotNull Firestore dataBase) throws ExecutionException, InterruptedException, ParseException {
        ApiFuture<QuerySnapshot> query = dataBase.collection(library.getName() + " books").get();

        QuerySnapshot querySnapshot = query.get();
        List<QueryDocumentSnapshot> documents = querySnapshot.getDocuments();

        List<PreBook> preBooks = new ArrayList<>();

        for (QueryDocumentSnapshot document : documents) {
            preBooks.add(unpackMapedBook(document.getData()));
        }

        query = dataBase.collection(library.getName() + " students").get();

        querySnapshot = query.get();
        documents = querySnapshot.getDocuments();

        List<Student> students = new ArrayList<>();

        for (QueryDocumentSnapshot document : documents) {
            PreStudent preStudent = unpackMapedStudent(document.getData());
            if (preStudent != null) {
                students.add(preStudent.getStudent());
            }
        }

        for (Student student : students) {
            library.addExistingStudent(student);
        }

        for (PreBook preBook : preBooks) {
            Book book = preBook.getBook();
            library.addExistingBook(book);
            if (preBook.getOwnerId() != null) {
                Student student = library.getStudentById(preBook.getOwnerId());
                book.giveTo(student, book.getTimeGiven());
            }
        }
    }

    public static PreBook downloadBookById(String libraryName, @NotNull UUID id, @NotNull Firestore dataBase) throws ExecutionException, InterruptedException, ParseException {
        ApiFuture<DocumentSnapshot> documentSnapshotApiFuture = dataBase.collection(libraryName + " books")
                .document("Book " + id.toString()).get();

        DocumentSnapshot documentSnapshot = documentSnapshotApiFuture.get();
        return unpackMapedBook(documentSnapshot.getData());

    }

    public static PreStudent downloadStudentById(String libraryName, @NotNull UUID id, @NotNull Firestore dataBase) throws ExecutionException, InterruptedException {
        ApiFuture<DocumentSnapshot> documentSnapshotApiFuture = dataBase.collection(libraryName + " students")
                .document("Student " + id.toString()).get();

        DocumentSnapshot documentSnapshot = documentSnapshotApiFuture.get();
        return unpackMapedStudent(documentSnapshot.getData());
    }

    public static List<BookTemplate>[] downloadCompectStandard(String libraryName, Firestore dataBase) throws ExecutionException, InterruptedException {
        List<BookTemplate>[] result = new List[11];
        for (int i = 0; i < 11; i++) {
            result[i] = new ArrayList<>();
        }
        ApiFuture<QuerySnapshot> query = dataBase.collection(libraryName + " standard").get();

        QuerySnapshot querySnapshot = query.get();
        List<QueryDocumentSnapshot> documents = querySnapshot.getDocuments();
        for (QueryDocumentSnapshot document : documents) {
            Map<String, Object> standardList = document.getData();
            int grade = toIntExact((Long) standardList.get("Grade"));
            List<Map<String, Object>> mapedTemplates = (ArrayList<Map<String, Object>>) standardList.get("Textbooks");
            for (Map<String, Object> p : mapedTemplates) {
                result[grade - 1].add(unpackMapedTemplate(p));
            }
        }
        return result;
    }

    public static Stats downloadStats(String libraryName, @NotNull Firestore dataBase) throws ExecutionException, InterruptedException {
        ApiFuture<DocumentSnapshot> documentSnapshotApiFuture = dataBase.collection(libraryName + " stats")
                .document("Current stats").get();

        DocumentSnapshot documentSnapshot = documentSnapshotApiFuture.get();
        return unpackMapedStats(documentSnapshot.getData());
    }

    public static Stats downloadStats(String libraryName, @NotNull Firestore dataBase, Date date) throws ExecutionException, InterruptedException {
        ApiFuture<DocumentSnapshot> documentSnapshotApiFuture = dataBase.collection(libraryName + " stats")
                .document("Stats on " + Library.DATE_FORMAT.format(date)).get();

        DocumentSnapshot documentSnapshot = documentSnapshotApiFuture.get();
        return unpackMapedStats(documentSnapshot.getData());
    }

    public static void deleteBookById(String libraryName, @NotNull Firestore dataBase, @NotNull UUID id) throws ExecutionException, InterruptedException {
        ApiFuture<WriteResult> writeResult = dataBase.collection(libraryName + " books")
                .document("Book " + id.toString()).delete();
        System.out.println("Update time : " + writeResult.get().getUpdateTime());
    }

    public static void deleteStudentById(String libraryName, @NotNull Firestore dataBase, @NotNull UUID id) throws ExecutionException, InterruptedException {
        ApiFuture<WriteResult> writeResult = dataBase.collection(libraryName + " students")
                .document("Student " + id.toString()).delete();
        System.out.println("Update time : " + writeResult.get().getUpdateTime());
    }

    public static void main(String[] args) throws IOException, InterruptedException, ExecutionException, ParseException {
//        Firestore dataBase = setUpFirebase();
//        Properties property = new Properties();
//        File prop = new File("C:\\Users\\Varamadon\\Documents\\Proga\\LibraryManager\\src\\main\\resources\\Names_ru.properties");
//        property.load(new FileReader(prop));
//        Library library = new Library(property.getProperty("libraryName"));
//
////        for (int i=0;i<11;i++) {
////            library.addSameBooks(i+1, 200, 2010,
////                    "Математика "+(i+1), "Васильев А.А.", "Европа",
////                    "Математика", 5+i);
////            library.addSameBooks(i+1, 220, 2015,
////                    "Русский язык "+(i+1), "Огурцов В.И.", "Азия",
////                    "Русский язык", 3+i);
////        }
////        for (int i=0;i<11;i++) {
////            for (int j=0;j<20;j++) {
////                library.addStudent(i+1,"Ученик клон номер "+(i+1)+(j+1));
////            }
////        }
////
////        List<BookTemplate>[] textbookComplectsPerGrade = new List[11];
////        for (int i=0;i<11;i++) {
////            textbookComplectsPerGrade[i] = new ArrayList<>();
////            textbookComplectsPerGrade[i].add(new BookTemplate(i+1,"Математика "+(i+1),
////                    "Васильев А.А.","Математика"));
////            textbookComplectsPerGrade[i].add(new BookTemplate(i+1,"Русский язык "+(i+1),
////                    "Огурцов В.И.","Русский язык"));
////        }
//
//        //uploadLibrary(library,dataBase);
//        //uploadCompectStandard(library.getName(),dataBase,textbookComplectsPerGrade);
//
////        Map<Book,UUID> preBook = downloadBookById(library.getName(),UUID.fromString("36062360-409a-435f-8d07-3f94f41f3b79"),dataBase);
////        for (Book book:preBook.keySet()
////             ) {
////            book.show();
////        }
//
//        List<BookTemplate>[] textbookComplectsPerGrade = downloadCompectStandard(library.getName(),dataBase);
//        downloadLibrary(library,dataBase);
//
//        for (List<BookTemplate> list:textbookComplectsPerGrade
//             ) {
//            for (BookTemplate bt:list
//                 ) {
//                if (library.getGroupByTemplate(bt)==null) {
//                    bt.show();
//                    continue;
//                }
//                System.out.println(library.getGroupByTemplate(bt).bookCount());
//            }
//        }
//
//
//        Stats stats = new Stats(library,textbookComplectsPerGrade);
//        uploadStats(library.getName(),dataBase,stats);


    }


}
