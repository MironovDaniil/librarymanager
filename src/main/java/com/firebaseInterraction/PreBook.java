package com.firebaseInterraction;

import com.library.Book;

import java.util.UUID;

/**
 * Created by Daniel Mironov on 22.09.2018
 */
public class PreBook {
    private final Book book;
    private final UUID ownerId;

    public PreBook(Book book, UUID ownerId) {
        this.book = book;
        this.ownerId = ownerId;
    }

    public Book getBook() {
        return book;
    }

    public UUID getOwnerId() {
        return ownerId;
    }
}
