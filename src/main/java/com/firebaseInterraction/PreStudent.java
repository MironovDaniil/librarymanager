package com.firebaseInterraction;

import com.library.Student;

import java.util.List;
import java.util.UUID;

/**
 * Created by Daniel Mironov on 22.09.2018
 */
public class PreStudent {
    private final Student student;
    private final List<UUID> ids;

    public PreStudent(Student student, List<UUID> ids) {
        this.student = student;
        this.ids = ids;
    }

    public Student getStudent() {
        return student;
    }

    public List<UUID> getIds() {
        return ids;
    }
}
