package com.userInterface;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;

import java.util.Optional;

import static com.firebaseInterraction.FirebaseMethods.uploadNewBookToStandart;

/**
 * Created by Daniel Mironov on 23.09.2018
 */
public class LibraryAdminController {
    private Stage dialogStage;

    private MainGUI mainGUI;

    @FXML
    private void initialize() {
    }

    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    @FXML
    private void handleClose() {
        dialogStage.close();
    }

    @FXML
    private void handleAddToStandart() throws Exception {
        if (mainGUI.getCurrentBook() == null) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Can not add textbook to standard!");
            alert.setContentText("Book is not selected!");

            alert.showAndWait();
            return;
        }
        if (mainGUI.getCurrentBook().getGrade()!=0) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Confirmation");
            alert.setHeaderText("Selected textbook will be added to standard textbook provision");
            alert.setContentText("Continue?");

            Optional<ButtonType> result = alert.showAndWait();
            if (result.isPresent()) {
                if (result.get() == ButtonType.OK){
                    uploadNewBookToStandart(mainGUI.getLibrary(),mainGUI.getDataBase(),mainGUI.getCurrentBook());
                    Alert alert1 = new Alert(Alert.AlertType.INFORMATION);
                    alert1.setTitle("Standard");
                    alert1.setHeaderText(null);
                    alert1.setContentText("Textbook was added to standard!");

                    alert1.showAndWait();
                }
            }
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Selected book is not a textbook");
            alert.setContentText("Can not add regular book to standard!");

            alert.showAndWait();
        }
    }

    public void setUp(MainGUI mainGUI) {
        this.mainGUI = mainGUI;
    }
}
