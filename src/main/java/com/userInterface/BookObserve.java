package com.userInterface;

import com.library.Book;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Created by Daniel Mironov on 22.09.2018
 */
public class BookObserve {
    private final Book value;

    public BookObserve(Book value) {
        this.value = value;
    }

    public Book getValue() {
        return value;
    }

    public StringProperty getObservableTitle () {
        return new SimpleStringProperty(value.getTitle());
    }

    public StringProperty getObservableAuthor() {
        return new SimpleStringProperty(value.getAuthorName());
    }

    public StringProperty getObservableHouse () {
        return new SimpleStringProperty(value.getPublishingHouse());
    }
}
