package com.userInterface;

import com.library.Book;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import static com.firebaseInterraction.FirebaseMethods.uploadBook;

/**
 * Created by Daniel Mironov on 23.09.2018
 */
public class BookAddController {
    @FXML
    private TextArea titleArea;
    @FXML
    private TextArea authorArea;
    @FXML
    private TextArea publishingHouseArea;
    @FXML
    private TextArea publishingYearArea;
    @FXML
    private TextArea pagesCountArea;
    @FXML
    private TextArea gradeArea;
    @FXML
    private TextArea subjectArea;
    @FXML
    private TextArea amountArea;
    @FXML
    private ProgressBar progressBar;

    private List<UUID> addIds = new ArrayList<>();

    private Stage dialogStage;

    private MainGUI mainGUI;

    @FXML
    private void initialize() {
    }

    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    @FXML
    private void handleClose() {
        dialogStage.close();
    }

    @FXML
    private void handleAdd() throws ExecutionException, InterruptedException, IOException {
        if (inputIsComplete()) {
            if (inputIsATextbook()) {
                if (inputIsValid()) {
                    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                    alert.setTitle("Confirmation");
                    alert.setHeaderText("Will be added " + amountArea.getText() + " textbooks");
                    alert.setContentText("Continue?");

                    Optional<ButtonType> result = alert.showAndWait();
                    if (result.isPresent()) {
                        if (result.get() == ButtonType.OK) {
                            addBooks();
                            Alert alert1 = new Alert(Alert.AlertType.INFORMATION);
                            alert1.setTitle("Textbooks added");
                            alert1.setHeaderText("Added " + addIds.size() + " textbooks. Warning! Do not loose IDs!");
                            alert1.setContentText("IDs of added textbooks:");

                            showIds(alert1);
                        }
                    }
                } else {
                    showInvalidInputError();
                }
            } else {
                if (inputIsValid()) {
                    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                    alert.setTitle("Confirmation");
                    alert.setHeaderText("Will be added " + amountArea.getText() + " books");
                    alert.setContentText("Continue?");

                    Optional<ButtonType> result = alert.showAndWait();
                    if (result.isPresent()) {
                        if (result.get() == ButtonType.OK) {
                            addBooks();
                            Alert alert1 = new Alert(Alert.AlertType.INFORMATION);
                            alert1.setTitle("Books added");
                            alert1.setHeaderText("Added " + addIds.size() + " books. Warning! Do not loose IDs!");
                            alert1.setContentText("IDs of added books:");

                            showIds(alert1);
                        }
                    }
                } else {
                    showInvalidInputError();
                }
            }
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Not all input data entered!");
            alert.setContentText("Fill all of the missing data and try again");

            alert.showAndWait();
        }
    }

    private void showIds(Alert alert1) {
        StringBuilder text = new StringBuilder();
        for (UUID id : addIds) {
            text.append(id.toString()).append("\n");
        }


        TextArea textArea = new TextArea("");
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        textArea.setText(text.toString());

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(textArea, 0, 1);

        alert1.getDialogPane().setExpandableContent(expContent);

        alert1.showAndWait();
        dialogStage.close();
    }

    private void showInvalidInputError() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText("Invalid input!");
        alert.setContentText("Correct input data and try again");

        alert.showAndWait();
    }

    private void addBooks() throws ExecutionException, InterruptedException, IOException {
        int amount = Integer.parseInt(amountArea.getText());
        String title = titleArea.getText();
        String author = authorArea.getText();
        String publishHouse = publishingHouseArea.getText();
        int publishYear = Integer.parseInt(publishingYearArea.getText());
        int pageCount = Integer.parseInt(pagesCountArea.getText());
        int grade;
        if (gradeArea.getText().equals("")) {
            grade = 0;
        } else {
            grade = Integer.parseInt(gradeArea.getText());
        }
        String subject = subjectArea.getText();
        for (int i = 0; i < amount; i++) {
            Book book = new Book(grade, pageCount, publishYear, title, author, publishHouse, subject, null);
            addIds.add(book.getId());
            uploadBook(mainGUI.getLibrary().getName(), mainGUI.getDataBase(), book);
        }

    }

    private boolean inputIsComplete() {
        return (!titleArea.getText().equals("")) &&
                (!authorArea.getText().equals("")) &&
                (!publishingHouseArea.getText().equals("")) &&
                (!publishingYearArea.getText().equals("")) &&
                (!pagesCountArea.getText().equals("")) &&
                (!amountArea.getText().equals(""));
    }

    private boolean inputIsATextbook() {
        return (!gradeArea.getText().equals("")) &&
                (!subjectArea.getText().equals(""));
    }

    private boolean inputIsValid() {
        return (publishingYearArea.getText().matches("\\d{4}")) &&
                (pagesCountArea.getText().matches("\\d+")) &&
                ((gradeArea.getText().equals("")) || ((Integer.parseInt(gradeArea.getText()) > 0) && (Integer.parseInt(gradeArea.getText()) < 12))) &&
                (amountArea.getText().matches("\\d+"));
    }

    private void showProgress() {
        progressBar.setProgress(-1);
    }

    public void setUp(MainGUI mainGUI) {
        this.mainGUI = mainGUI;
        progressBar.setProgress(0);
    }


}
