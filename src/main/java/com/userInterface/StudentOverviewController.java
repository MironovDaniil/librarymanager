package com.userInterface;

import com.library.Book;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import static com.firebaseInterraction.FirebaseMethods.deleteStudentById;

/**
 * Created by Daniel Mironov on 22.09.2018
 */
public class StudentOverviewController {
    @FXML
    private TextField nameField;
    @FXML
    private TextField gradeField;
    @FXML
    private TableView<BookObserve> bookTable;
    @FXML
    private TableColumn<BookObserve, String> titleColumn;
    @FXML
    private TableColumn<BookObserve, String> authorColumn;
    @FXML
    private TableColumn<BookObserve, String> publishHouseColumn;

    private Stage dialogStage;

    private MainGUI mainGUI;

    private Book selectedBook;

    @FXML
    private void initialize() {
        titleColumn.setCellValueFactory(c -> c.getValue().getObservableTitle());
        authorColumn.setCellValueFactory(c->c.getValue().getObservableAuthor());
        publishHouseColumn.setCellValueFactory(c->c.getValue().getObservableHouse());

        bookTable.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> selectedBook = newValue.getValue());
    }

    @FXML
    private void handleClose() {
        dialogStage.close();
    }

    @FXML
    private void handleGoToBook() {
        Book oldBook = mainGUI.getCurrentBook();
        if (!selectedBook.equals(oldBook)) {
            mainGUI.setCurrentBook(selectedBook);
        }
        try {
            // Загружаем fxml-файл и создаём новую сцену
            // для всплывающего диалогового окна.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainGUI.class.getResource("/BookOverview.fxml"));
            AnchorPane page = loader.load();
            //rootLayout.setCenter(page);

            // Создаём диалоговое окно Stage.
            Stage dialogStage = new Stage();
            dialogStage.initModality(Modality.APPLICATION_MODAL);
            dialogStage.initOwner(mainGUI.getPrimaryStage());
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Передаём адресата в контроллер.
            BookOverviewController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setUp(mainGUI);

            dialogStage.showAndWait();
            mainGUI.setCurrentBook(oldBook);
            bookTable.setItems(mainGUI.getCurrentStudentsBooks());
            show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void handleDelete() {
        if (mainGUI.getCurrentStudent().getCurrentBooksOnHands().size()!=0) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Can not expel student!");
            alert.setContentText("This student has some books!");

            alert.showAndWait();
            return;
        }
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Confirmation");
        alert.setHeaderText("Student will be expelled. Warning, this can not be undone!");
        alert.setContentText("Continue?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent()) {
            if (result.get() == ButtonType.OK){
                try {
                    deleteStudentById(mainGUI.getLibrary().getName(),mainGUI.getDataBase(),mainGUI.getCurrentStudent().getId());
                    if (mainGUI.isSyncWithDB()) {
                        mainGUI.getLibrary().deleteStudent(mainGUI.getCurrentStudent());
                    }
                    mainGUI.nullifyCurrentStudent();
                    dialogStage.close();
                } catch (ExecutionException | InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @FXML
    private void show() {
        if (mainGUI.getCurrentStudent()==null) {
            nameField.setText("");
            gradeField.setText("");
        } else {
            nameField.setText(mainGUI.getCurrentStudent().getName());

            gradeField.setText(String.valueOf(mainGUI.getCurrentStudent().getGrade()));

        }
    }

    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    public void setUp(MainGUI mainGUI) {
        this.mainGUI = mainGUI;

        bookTable.setItems(mainGUI.getCurrentStudentsBooks());

        show();
    }
}
