package com.userInterface;

import com.library.Book;
import com.library.Library;
import com.library.Student;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;
import org.jetbrains.annotations.NotNull;

import java.util.Date;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import static com.firebaseInterraction.FirebaseMethods.deleteBookById;
import static com.firebaseInterraction.FirebaseMethods.uploadBook;
import static com.firebaseInterraction.FirebaseMethods.uploadStudent;

public class BookOverviewController {


    @FXML
    private TextArea titleArea;
    @FXML
    private TextArea authorArea;
    @FXML
    private TextArea publishingHouseArea;
    @FXML
    private TextArea publishingYearArea;
    @FXML
    private TextArea pagesCountArea;
    @FXML
    private TextArea gradeArea;
    @FXML
    private TextArea subjectArea;
    @FXML
    private TextArea handlerArea;
    @FXML
    private TextArea dateGivenArea;

    private Stage dialogStage;

    private MainGUI mainGUI;

    @FXML
    private void initialize() {
    }

    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    @FXML
    private void handleClose() {
        dialogStage.close();
    }

    @FXML
    private void handleReturn() {
        Book book = mainGUI.getCurrentBook();
        if (book.getCurrentHandler()==null) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Can not return book!");
            alert.setHeaderText(null);
            alert.setContentText("Book is already in the library!");

            alert.showAndWait();
        } else {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Book returning");
            alert.setHeaderText("Book will be returned to the library");
            alert.setContentText("Continue?");

            Optional<ButtonType> result = alert.showAndWait();
            if (result.isPresent()) {
                if (result.get() == ButtonType.OK){
                    Student student = book.getCurrentHandler();
                    book.returnToLibrary();
                    try {
                        uploadBook(mainGUI.getLibrary().getName(),mainGUI.getDataBase(),book);
                        uploadStudent(mainGUI.getLibrary().getName(),mainGUI.getDataBase(),student);
                    } catch (ExecutionException | InterruptedException e) {
                        e.printStackTrace();
                    }
                    Alert alert1 = new Alert(Alert.AlertType.INFORMATION);
                    alert1.setTitle("Finished!");
                    alert1.setHeaderText(null);
                    alert1.setContentText("Book was returned to the library!");
                    showBookDetails();

                    alert1.showAndWait();
                }
            }
        }
    }

    @FXML
    private void handleGive () {
        Book book = mainGUI.getCurrentBook();
        if (book.getCurrentHandler()!=null) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Can not give book");
            alert.setContentText("Student has that book!");

            alert.showAndWait();
        } else {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Book giving");
            alert.setHeaderText("Book will be given to the selected student");
            alert.setContentText("Are you sure?");

            Optional<ButtonType> result = alert.showAndWait();
            if (result.isPresent()) {
                if (result.get() == ButtonType.OK){
                    Student student = mainGUI.getCurrentStudent();
                    book.giveTo(student);
                    try {
                        uploadBook(mainGUI.getLibrary().getName(),mainGUI.getDataBase(),book);
                        uploadStudent(mainGUI.getLibrary().getName(),mainGUI.getDataBase(),student);
                    } catch (ExecutionException | InterruptedException e) {
                        e.printStackTrace();
                    }
                    Alert alertFinish = new Alert(Alert.AlertType.INFORMATION);
                    alertFinish.setTitle("Finished");
                    alertFinish.setHeaderText(null);
                    alertFinish.setContentText("Book was given to the selected student!");
                    showBookDetails();

                    alertFinish.showAndWait();
                }
            }
        }
    }

    @FXML
    private void handleDelete() {
        if (mainGUI.getCurrentBook().getCurrentHandler()!=null) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Can not withdraw book");
            alert.setContentText("Student has that book!");

            alert.showAndWait();
            return;
        }
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Confirmation");
        alert.setHeaderText("Book will be withdrawn. Warning! This can not be undone!");
        alert.setContentText("Continue?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent()) {
            if (result.get() == ButtonType.OK){
                try {
                    deleteBookById(mainGUI.getLibrary().getName(),mainGUI.getDataBase(),mainGUI.getCurrentBook().getId());
                    if (mainGUI.isSyncWithDB()) {
                        mainGUI.getLibrary().deleteBook(mainGUI.getCurrentBook());
                    }
                    mainGUI.nullifyCurrentBook();
                    dialogStage.close();
                } catch (ExecutionException | InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @FXML
    private void showBookDetails() {
        if (mainGUI.getCurrentBook() != null) {
            titleArea.setText(mainGUI.getCurrentBook().getTitle());
            authorArea.setText(mainGUI.getCurrentBook().getAuthorName());
            publishingHouseArea.setText(mainGUI.getCurrentBook().getPublishingHouse());
            publishingYearArea.setText(String.valueOf(mainGUI.getCurrentBook().getPublishYear()));
            pagesCountArea.setText(String.valueOf(mainGUI.getCurrentBook().getPageCount()));
            gradeArea.setText(String.valueOf(mainGUI.getCurrentBook().getGrade()));
            subjectArea.setText(mainGUI.getCurrentBook().getSubject());
            if (Library.DATE_FORMAT.format(mainGUI.getCurrentBook().getTimeGiven())
                    .equals(Library.DATE_FORMAT.format(new Date(10)))) {
                dateGivenArea.setText("Book is in library");
            } else {
                dateGivenArea.setText(Library.DATE_FORMAT.format(mainGUI.getCurrentBook().getTimeGiven()));
            }
            if (mainGUI.getCurrentBook().getCurrentHandler() == null) {
                handlerArea.setText("Book is in library");
            } else {
                handlerArea.setText(mainGUI.getCurrentStudent().getName()
                        + " from " + mainGUI.getCurrentStudent().getGrade() + " grade");
            }

        } else {
            titleArea.setText("");
            authorArea.setText("");
            publishingHouseArea.setText("");
            publishingYearArea.setText("");
            pagesCountArea.setText("");
            gradeArea.setText("");
            subjectArea.setText("");
            dateGivenArea.setText("");
            handlerArea.setText("");
        }
    }

    public void setUp(MainGUI mainGUI) {
        this.mainGUI = mainGUI;
        showBookDetails();
    }
}
