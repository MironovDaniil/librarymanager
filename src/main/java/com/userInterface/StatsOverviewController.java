package com.userInterface;

import com.library.BookTemplate;
import com.library.Library;
import com.library.Stats;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import static com.firebaseInterraction.FirebaseMethods.*;

public class StatsOverviewController {

    private static DecimalFormat df = new DecimalFormat("#.####");

    @FXML
    private TextField studentsPerGradeField1;
    @FXML
    private TextField studentsPerGradeField2;
    @FXML
    private TextField studentsPerGradeField3;
    @FXML
    private TextField studentsPerGradeField4;
    @FXML
    private TextField studentsPerGradeField5;
    @FXML
    private TextField studentsPerGradeField6;
    @FXML
    private TextField studentsPerGradeField7;
    @FXML
    private TextField studentsPerGradeField8;
    @FXML
    private TextField studentsPerGradeField9;
    @FXML
    private TextField studentsPerGradeField10;
    @FXML
    private TextField studentsPerGradeField11;

    @FXML
    private TextField textbookPerGradeField1;
    @FXML
    private TextField textbookPerGradeField2;
    @FXML
    private TextField textbookPerGradeField3;
    @FXML
    private TextField textbookPerGradeField4;
    @FXML
    private TextField textbookPerGradeField5;
    @FXML
    private TextField textbookPerGradeField6;
    @FXML
    private TextField textbookPerGradeField7;
    @FXML
    private TextField textbookPerGradeField8;
    @FXML
    private TextField textbookPerGradeField9;
    @FXML
    private TextField textbookPerGradeField10;
    @FXML
    private TextField textbookPerGradeField11;

    @FXML
    private TextField percentagePerGradeField1;
    @FXML
    private TextField percentagePerGradeField2;
    @FXML
    private TextField percentagePerGradeField3;
    @FXML
    private TextField percentagePerGradeField4;
    @FXML
    private TextField percentagePerGradeField5;
    @FXML
    private TextField percentagePerGradeField6;
    @FXML
    private TextField percentagePerGradeField7;
    @FXML
    private TextField percentagePerGradeField8;
    @FXML
    private TextField percentagePerGradeField9;
    @FXML
    private TextField percentagePerGradeField10;
    @FXML
    private TextField percentagePerGradeField11;

    @FXML
    private TextField studentCountField;
    @FXML
    private TextField bookCountField;

    private Stage dialogStage;

    private MainGUI mainGUI;

    private Stats currentStats;

    @FXML
    private void initialize() {
    }

    @FXML
    private void handleClose() {
        dialogStage.close();
    }

    @FXML
    private void handleSetDate() {
        TextInputDialog dialog = new TextInputDialog("");
        dialog.setTitle("Статистика за дату");
        dialog.setHeaderText("Введите дату в формате <<дд-мм-гггг>> без лишних пробелов. Например: <<03-06-1996>> ");
        dialog.setContentText("Введите дату:");


        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()){
            String input = result.get();
            try {
                Date date = Library.DATE_FORMAT.parse(input);
                Stats stats = downloadStats(mainGUI.getLibrary().getName(),mainGUI.getDataBase(),date);
                if (stats == null) {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Ошибка");
                    alert.setHeaderText("Неверная дата!");
                    alert.initOwner(dialogStage);
                    alert.setContentText("Не сохранено статистики за искомую дату");

                    alert.showAndWait();
                }
                if (stats!=null) {
                    currentStats = stats;
                    showStats();
                }

            } catch (ParseException e) {

                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Ошибка");
                alert.setHeaderText("Неверный формат даты!");
                alert.initOwner(dialogStage);
                alert.setContentText("Введите дату заново");

                alert.showAndWait();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }

    }

    @FXML
    private void handleRecalc() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation");
        alert.setHeaderText("Stats calculation might take some time");
        alert.setContentText("Continue?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent()) {
            if (result.get() == ButtonType.OK){
                try {
                    mainGUI.syncWithDB();
                    List<BookTemplate>[] textbookComplectsPerGrade =
                            downloadCompectStandard(mainGUI.getLibrary().getName(),mainGUI.getDataBase());
                    Stats stats = new Stats(mainGUI.getLibrary(),textbookComplectsPerGrade);
                    currentStats = stats;
                    uploadStats(mainGUI.getLibrary().getName(),mainGUI.getDataBase(),stats);
                    showStats();
                    Alert alert1 = new Alert(Alert.AlertType.INFORMATION);
                    alert1.setTitle("Finished");
                    alert1.setHeaderText(null);
                    alert1.setContentText("Finished!");

                    alert1.showAndWait();
                } catch (ExecutionException | InterruptedException | ParseException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    @FXML
    private void showStats() {
        studentCountField.setText(String.valueOf(currentStats.getStudentsCount()));
        bookCountField.setText(String.valueOf(currentStats.getBookCount()));

        studentsPerGradeField1.setText(String.valueOf(currentStats.getStudentsPerGradeCount()[0]));
        studentsPerGradeField2.setText(String.valueOf(currentStats.getStudentsPerGradeCount()[1]));
        studentsPerGradeField3.setText(String.valueOf(currentStats.getStudentsPerGradeCount()[2]));
        studentsPerGradeField4.setText(String.valueOf(currentStats.getStudentsPerGradeCount()[3]));
        studentsPerGradeField5.setText(String.valueOf(currentStats.getStudentsPerGradeCount()[4]));
        studentsPerGradeField6.setText(String.valueOf(currentStats.getStudentsPerGradeCount()[5]));
        studentsPerGradeField7.setText(String.valueOf(currentStats.getStudentsPerGradeCount()[6]));
        studentsPerGradeField8.setText(String.valueOf(currentStats.getStudentsPerGradeCount()[7]));
        studentsPerGradeField9.setText(String.valueOf(currentStats.getStudentsPerGradeCount()[8]));
        studentsPerGradeField10.setText(String.valueOf(currentStats.getStudentsPerGradeCount()[9]));
        studentsPerGradeField11.setText(String.valueOf(currentStats.getStudentsPerGradeCount()[10]));

        textbookPerGradeField1.setText(String.valueOf(currentStats.getTextbookPerGradeCount()[0]));
        textbookPerGradeField2.setText(String.valueOf(currentStats.getTextbookPerGradeCount()[1]));
        textbookPerGradeField3.setText(String.valueOf(currentStats.getTextbookPerGradeCount()[2]));
        textbookPerGradeField4.setText(String.valueOf(currentStats.getTextbookPerGradeCount()[3]));
        textbookPerGradeField5.setText(String.valueOf(currentStats.getTextbookPerGradeCount()[4]));
        textbookPerGradeField6.setText(String.valueOf(currentStats.getTextbookPerGradeCount()[5]));
        textbookPerGradeField7.setText(String.valueOf(currentStats.getTextbookPerGradeCount()[6]));
        textbookPerGradeField8.setText(String.valueOf(currentStats.getTextbookPerGradeCount()[7]));
        textbookPerGradeField9.setText(String.valueOf(currentStats.getTextbookPerGradeCount()[8]));
        textbookPerGradeField10.setText(String.valueOf(currentStats.getTextbookPerGradeCount()[9]));
        textbookPerGradeField11.setText(String.valueOf(currentStats.getTextbookPerGradeCount()[10]));

        percentagePerGradeField1.setText(String.valueOf(df.format(currentStats.getTextbookPercentagePerGrade()[0] * 100)));
        percentagePerGradeField2.setText(String.valueOf(df.format(currentStats.getTextbookPercentagePerGrade()[1] * 100)));
        percentagePerGradeField3.setText(String.valueOf(df.format(currentStats.getTextbookPercentagePerGrade()[2] * 100)));
        percentagePerGradeField4.setText(String.valueOf(df.format(currentStats.getTextbookPercentagePerGrade()[3] * 100)));
        percentagePerGradeField5.setText(String.valueOf(df.format(currentStats.getTextbookPercentagePerGrade()[4] * 100)));
        percentagePerGradeField6.setText(String.valueOf(df.format(currentStats.getTextbookPercentagePerGrade()[5] * 100)));
        percentagePerGradeField7.setText(String.valueOf(df.format(currentStats.getTextbookPercentagePerGrade()[6] * 100)));
        percentagePerGradeField8.setText(String.valueOf(df.format(currentStats.getTextbookPercentagePerGrade()[7] * 100)));
        percentagePerGradeField9.setText(String.valueOf(df.format(currentStats.getTextbookPercentagePerGrade()[8] * 100)));
        percentagePerGradeField10.setText(String.valueOf(df.format(currentStats.getTextbookPercentagePerGrade()[9] * 100)));
        percentagePerGradeField11.setText(String.valueOf(df.format(currentStats.getTextbookPercentagePerGrade()[10] * 100)));
    }

    public void setUp(MainGUI mainGUI) {
        this.mainGUI = mainGUI;
        try {
            currentStats = downloadStats(this.mainGUI.getLibrary().getName(), this.mainGUI.getDataBase());
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        showStats();
    }

}
