package com.userInterface;

import com.google.cloud.firestore.Firestore;
import com.library.Book;
import com.library.Library;
import com.library.Stats;
import com.library.Student;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import static com.firebaseInterraction.FirebaseMethods.*;

public class MainGUI extends Application {

    private Library library;
    private Firestore dataBase;
    private Book currentBook;
    private Student currentStudent;
    private Stage primaryStage;
    private BorderPane rootLayout;
    private boolean isSyncWithDB = false;

    public MainGUI() {
    }


    @Override
    public void start(Stage primaryStage) {
        Properties property = new Properties();
        String resourceName = "Names.properties";
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        try (InputStream resourceStream = loader.getResourceAsStream(resourceName)) {
            property.load(resourceStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            dataBase = setUpFirebase();
        } catch (IOException e) {
            e.printStackTrace();
        }
        library = new Library(property.getProperty("libraryName"));
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Library manager");

//        List[] ololo = new List[11];
//        for(int i=0;i<11;i++) {
//            ololo[i] = new ArrayList();
//        }
//
//        try {
//            uploadStats(library.getName(), dataBase, new Stats());
//            uploadCompectStandard(library, dataBase, ololo);
//        } catch (ExecutionException e) {
//            e.printStackTrace();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

        initRootLayout();

        showHomePage();

    }

    public void initRootLayout() {
        try {
            // Загружаем корневой макет из fxml файла.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainGUI.class.getResource("/RootLayout.fxml"));
            rootLayout = loader.load();

            // Отображаем сцену, содержащую корневой макет.
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void syncWithDB() throws InterruptedException, ExecutionException, ParseException {
        downloadLibrary(library, dataBase);
        isSyncWithDB = true;
    }

    public void asSync() {
        isSyncWithDB = false;
    }

    public void nullifyCurrentBook() {
        currentBook = null;
    }

    public void nullifyCurrentStudent() {
        currentStudent = null;
    }

    public void showHomePage() {
        try {
            // Загружаем сведения об адресатах.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainGUI.class.getResource("/HomePage.fxml"));
            AnchorPane homePage = loader.load();
            HomePageController controller = loader.getController();
            controller.setUp(this);

            // Помещаем сведения об адресатах в центр корневого макета.
            rootLayout.setCenter(homePage);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public Library getLibrary() {
        return library;
    }

    public Firestore getDataBase() {
        return dataBase;
    }

    public Book getCurrentBook() {
        return currentBook;
    }

    public Student getCurrentStudent() {
        return currentStudent;
    }

    public ObservableList<BookObserve> getCurrentStudentsBooks() {
        ObservableList<BookObserve> result = FXCollections.observableArrayList();
        List<BookObserve> bookObserveList = currentStudent.getCurrentBooksOnHands().stream().map(BookObserve::new).collect(Collectors.toList());
        result.addAll(bookObserveList);
        return result;
    }

    public void setCurrentBook(Book currentBook) {
        this.currentBook = currentBook;
    }

    public void setCurrentStudent(Student currentStudent) {
        this.currentStudent = currentStudent;
    }

    public boolean isSyncWithDB() {
        return isSyncWithDB;
    }


    public static void main(String[] args) {
        launch(args);
    }

}
