package com.userInterface;

import com.library.Student;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.util.UUID;
import java.util.concurrent.ExecutionException;

import static com.firebaseInterraction.FirebaseMethods.uploadStudent;

/**
 * Created by Daniel Mironov on 23.09.2018
 */
public class StudentAddController {
    @FXML
    private TextField nameField;
    @FXML
    private TextField gradeField;

    private UUID addId;

    private Stage dialogStage;

    private MainGUI mainGUI;

    @FXML
    private void initialize() {
    }

    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    @FXML
    private void handleClose() {
        dialogStage.close();
    }

    @FXML
    private void handleAdd() throws ExecutionException, InterruptedException {
        if (inputIsComplete()) {
            if (inputIsValid()) {
                String name = nameField.getText();
                int grade = Integer.parseInt(gradeField.getText());
                Student student = new Student(grade,name);
                addId = student.getId();
                uploadStudent(mainGUI.getLibrary().getName(),mainGUI.getDataBase(),student);
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Student was added");
                alert.setHeaderText("Warning, do not loose ID!");
                alert.setContentText("Student's ID:");

                TextField textField = new TextField(addId.toString());
                textField.setEditable(false);
                alert.getDialogPane().setContent(textField);

                alert.showAndWait();
                dialogStage.close();
            } else {
                showInvalidInputError();
            }
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Not all input data entered!");
            alert.setContentText("Enter all of the missing input data and try again");

            alert.showAndWait();
        }
    }

    private void showInvalidInputError() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText("Invalid input!");
        alert.setContentText("Correct input data and try again");

        alert.showAndWait();
    }

    private boolean inputIsValid() {
        return ((Integer.parseInt(gradeField.getText()) > 0)
                && (Integer.parseInt(gradeField.getText()) < 12));
    }

    private boolean inputIsComplete() {
        return (!nameField.getText().equals("")) &&
                (!gradeField.getText().equals(""));
    }

    public void setUp(MainGUI mainGUI) {
        this.mainGUI = mainGUI;
    }
}
