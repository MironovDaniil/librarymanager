package com.userInterface;

import com.firebaseInterraction.PreBook;
import com.firebaseInterraction.PreStudent;
import com.library.Book;
import com.library.Student;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import static com.firebaseInterraction.FirebaseMethods.downloadBookById;
import static com.firebaseInterraction.FirebaseMethods.downloadStudentById;

public class HomePageController {

    private MainGUI mainGUI;

    @FXML
    private void initialize() {
    }

    @FXML
    private void handleShowStudent() {
        if (mainGUI.getCurrentStudent() == null) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Can not show student!");
            alert.setContentText("No such student! Perhaps he was expelled");

            alert.showAndWait();
            return;
        }
        try {
            // Загружаем fxml-файл и создаём новую сцену
            // для всплывающего диалогового окна.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainGUI.class.getResource("/StudentOverview.fxml"));
            AnchorPane page = loader.load();
            //rootLayout.setCenter(page);

            // Создаём диалоговое окно Stage.
            Stage dialogStage = new Stage();
            dialogStage.initModality(Modality.APPLICATION_MODAL);
            dialogStage.initOwner(mainGUI.getPrimaryStage());
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Передаём адресата в контроллер.
            StudentOverviewController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setUp(mainGUI);

            dialogStage.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void handleShowBook() {
        if (mainGUI.getCurrentBook() == null) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Can not show book!");
            alert.setContentText("No such book! Perhaps it was withdrawn");

            alert.showAndWait();
            return;
        }
        try {
            // Загружаем fxml-файл и создаём новую сцену
            // для всплывающего диалогового окна.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainGUI.class.getResource("/BookOverview.fxml"));
            AnchorPane page = (AnchorPane) loader.load();
            //rootLayout.setCenter(page);

            // Создаём диалоговое окно Stage.
            Stage dialogStage = new Stage();
            dialogStage.initModality(Modality.APPLICATION_MODAL);
            dialogStage.initOwner(mainGUI.getPrimaryStage());
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Передаём адресата в контроллер.
            BookOverviewController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setUp(mainGUI);

            dialogStage.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void handleAddBook() {
        try {
            // Загружаем fxml-файл и создаём новую сцену
            // для всплывающего диалогового окна.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainGUI.class.getResource("/BookAdd.fxml"));
            AnchorPane page = loader.load();
            //rootLayout.setCenter(page);

            // Создаём диалоговое окно Stage.
            Stage dialogStage = new Stage();
            dialogStage.initModality(Modality.APPLICATION_MODAL);
            dialogStage.initOwner(mainGUI.getPrimaryStage());
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Передаём адресата в контроллер.
            BookAddController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setUp(mainGUI);

            dialogStage.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void handleAddStudent() {
        try {
            // Загружаем fxml-файл и создаём новую сцену
            // для всплывающего диалогового окна.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainGUI.class.getResource("/StudentAdd.fxml"));
            AnchorPane page = loader.load();
            //rootLayout.setCenter(page);

            // Создаём диалоговое окно Stage.
            Stage dialogStage = new Stage();
            dialogStage.initModality(Modality.APPLICATION_MODAL);
            dialogStage.initOwner(mainGUI.getPrimaryStage());
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Передаём адресата в контроллер.
            StudentAddController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setUp(mainGUI);

            dialogStage.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void handleShowStats() {
        try {
            // Загружаем fxml-файл и создаём новую сцену
            // для всплывающего диалогового окна.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainGUI.class.getResource("/StatsOverview.fxml"));
            AnchorPane page = loader.load();
            //rootLayout.setCenter(page);

            // Создаём диалоговое окно Stage.
            Stage dialogStage = new Stage();
            dialogStage.initModality(Modality.APPLICATION_MODAL);
            dialogStage.initOwner(mainGUI.getPrimaryStage());
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Передаём адресата в контроллер.
            StatsOverviewController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setUp(mainGUI);

            dialogStage.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void handleSelectBook() throws InterruptedException, ExecutionException, ParseException {
        TextInputDialog dialog = new TextInputDialog("");
        dialog.initStyle(StageStyle.UTILITY);
        dialog.setTitle("Input");
        dialog.setHeaderText("Select book");
        dialog.setContentText("Enter ID of the book:");
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            UUID uuid = UUID.fromString(result.get());
            PreBook preBook = downloadBookById(mainGUI.getLibrary().getName(),
                    uuid, mainGUI.getDataBase());
            UUID handlerId;
            mainGUI.setCurrentBook(preBook.getBook());
            handlerId = preBook.getOwnerId();
            if (handlerId != null) {
                PreStudent preStudent = downloadStudentById(mainGUI.getLibrary().getName(), handlerId, mainGUI.getDataBase());
                Student student = preStudent.getStudent();
                mainGUI.getCurrentBook().giveTo(student, mainGUI.getCurrentBook().getTimeGiven());
            }
        }
    }

    @FXML
    private void handleSelectStudent() throws ExecutionException, InterruptedException, ParseException {
        TextInputDialog dialog = new TextInputDialog("");
        dialog.initStyle(StageStyle.UTILITY);
        dialog.setTitle("Input");
        dialog.setHeaderText("Select student");
        dialog.setContentText("Enter ID of the student:");
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            UUID uuid = UUID.fromString(result.get());
            PreStudent preStudent = downloadStudentById(mainGUI.getLibrary().getName(),
                    uuid, mainGUI.getDataBase());
            mainGUI.setCurrentStudent(preStudent.getStudent());
            List<UUID> bookIds = preStudent.getIds();
            for (UUID id:bookIds) {
                PreBook preBook1 = downloadBookById(mainGUI.getLibrary().getName(),id,mainGUI.getDataBase());
                Book book = preBook1.getBook();
                book.giveTo(mainGUI.getCurrentStudent());
            }
        }
    }

    @FXML
    private void handleAdmin() {
        try {
            // Загружаем fxml-файл и создаём новую сцену
            // для всплывающего диалогового окна.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainGUI.class.getResource("/LibraryAdmin.fxml"));
            AnchorPane page = loader.load();
            //rootLayout.setCenter(page);

            // Создаём диалоговое окно Stage.
            Stage dialogStage = new Stage();
            dialogStage.initModality(Modality.APPLICATION_MODAL);
            dialogStage.initOwner(mainGUI.getPrimaryStage());
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Передаём адресата в контроллер.
            LibraryAdminController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setUp(mainGUI);

            dialogStage.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void handleHelp() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Contacts for help");
        alert.setHeaderText("With all your questions please send email on listed address");
        alert.setContentText("Email address:");



        TextArea textArea = new TextArea("varamadon@yandex.ru");
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);


        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(textArea, 0, 1);

        alert.getDialogPane().setContent(expContent);

        alert.showAndWait();
    }

    public void setUp(MainGUI mainGUI) {
        this.mainGUI = mainGUI;
    }
}
