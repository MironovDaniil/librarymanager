package com.library;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.*;

public class Stats {
    private final int studentsCount;
    private final int[] studentsPerGradeCount;
    private final int bookCount;
    private final int[] textbookPerGradeCount;
    private final List<BookTemplate>[] textbookComplectsPerGrade;
    private final double[] textbookPercentagePerGrade;

    public Stats() {
        studentsCount = 0;
        studentsPerGradeCount = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        bookCount = 0;
        textbookPerGradeCount = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        textbookComplectsPerGrade = null;
        textbookPercentagePerGrade = new double[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    }

    public Stats(int studentsCount, int[] studentsPerGradeCount, int bookCount, int[] textbookPerGradeCount,
                 List<BookTemplate>[] textbookComplectsPerGrade, double[] textbookPercentagePerGrade) {
        this.studentsCount = studentsCount;
        this.studentsPerGradeCount = studentsPerGradeCount;
        this.bookCount = bookCount;
        this.textbookPerGradeCount = textbookPerGradeCount;
        this.textbookComplectsPerGrade = textbookComplectsPerGrade;
        this.textbookPercentagePerGrade = textbookPercentagePerGrade;
    }

    public Stats(@NotNull Library library, List<BookTemplate>[] textbookComplectsPerGrade) {
        this.textbookComplectsPerGrade = textbookComplectsPerGrade;
        studentsCount = library.getStudents().size();
        studentsPerGradeCount = new int[11];
        for (int i = 0; i < 11; i++) {
            studentsPerGradeCount[i] = library.countStudentsOnParallel(i + 1);
        }
        bookCount = library.getBooksPerId().size();
        textbookPerGradeCount = new int[11];
        for (int i = 0; i < 11; i++) {
            textbookPerGradeCount[i] = library.countTextbooksPerGrade(i + 1);
        }
        textbookPercentagePerGrade = new double[11];
        for (int i = 0; i < 11; i++) {
            textbookPercentagePerGrade[i] = ((double) library.countComplects(this.textbookComplectsPerGrade[i]))
                    / ((double) studentsPerGradeCount[i]);
        }
    }

    public Stats(@NotNull Library library) {
        this.textbookComplectsPerGrade = null;
        studentsCount = library.getStudents().size();
        studentsPerGradeCount = new int[11];
        for (int i = 0; i < 11; i++) {
            studentsPerGradeCount[i] = library.countStudentsOnParallel(i + 1);
        }
        bookCount = library.getBooksPerId().size();
        textbookPerGradeCount = new int[11];
        for (int i = 0; i < 11; i++) {
            textbookPerGradeCount[i] = library.countTextbooksPerGrade(i + 1);
        }
        textbookPercentagePerGrade = new double[11];
        for (int i = 0; i < 11; i++) {
            textbookPercentagePerGrade[i] = 0;
        }
    }

    public Map<String, Object> packToMap() {
        Map<String, Object> result = new HashMap<>(5);
        result.put("studentsCount", studentsCount);
        List<Integer> studentsPerGradeCountList = new ArrayList<>(11);
        for (int i : studentsPerGradeCount
        ) {
            studentsPerGradeCountList.add(i);
        }
        result.put("studentsPerGradeCount", studentsPerGradeCountList);
        result.put("bookCount", bookCount);
        List<Integer> textbookPerGradeCountList = new ArrayList<>(11);
        for (int i : textbookPerGradeCount
        ) {
            textbookPerGradeCountList.add(i);
        }
        result.put("textbookPerGradeCount", textbookPerGradeCountList);
        List<Double> textbookPercentagePerGradeList = new ArrayList<>(11);
        for (double i : textbookPercentagePerGrade
        ) {
            textbookPercentagePerGradeList.add(i);
        }
        result.put("textbookPercentagePerGrade", textbookPercentagePerGradeList);
        return result;
    }

    public void show() {
        System.out.println("studentsCount " + studentsCount);
        System.out.println("bookCount " + bookCount);
        for (int i = 0; i < 11; i++) {
            System.out.println("Students in grade " + (i + 1) + " : " + studentsPerGradeCount[i]);
        }
        for (int i = 0; i < 11; i++) {
            System.out.println("Textbooks in grade " + (i + 1) + " : " + textbookPerGradeCount[i]);
        }
        for (int i = 0; i < 11; i++) {
            System.out.println("TextbooksPercentage in grade " + (i + 1) + " : " + textbookPercentagePerGrade[i]);
        }
    }

    public int getStudentsCount() {
        return studentsCount;
    }

    public int[] getStudentsPerGradeCount() {
        return studentsPerGradeCount;
    }

    public int getBookCount() {
        return bookCount;
    }

    public int[] getTextbookPerGradeCount() {
        return textbookPerGradeCount;
    }

    public List<BookTemplate>[] getTextbookComplectsPerGrade() {
        return textbookComplectsPerGrade;
    }

    public double[] getTextbookPercentagePerGrade() {
        return textbookPercentagePerGrade;
    }

    @Contract(value = "null -> false", pure = true)
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Stats)) return false;
        Stats stats = (Stats) o;
        return getStudentsCount() == stats.getStudentsCount() &&
                getBookCount() == stats.getBookCount() &&
                Arrays.equals(getStudentsPerGradeCount(), stats.getStudentsPerGradeCount()) &&
                Arrays.equals(getTextbookPerGradeCount(), stats.getTextbookPerGradeCount()) &&
                Arrays.equals(getTextbookComplectsPerGrade(), stats.getTextbookComplectsPerGrade()) &&
                Arrays.equals(getTextbookPercentagePerGrade(), stats.getTextbookPercentagePerGrade());
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(getStudentsCount(), getBookCount());
        result = 31 * result + Arrays.hashCode(getStudentsPerGradeCount());
        result = 31 * result + Arrays.hashCode(getTextbookPerGradeCount());
        result = 31 * result + Arrays.hashCode(getTextbookComplectsPerGrade());
        result = 31 * result + Arrays.hashCode(getTextbookPercentagePerGrade());
        return result;
    }
}
