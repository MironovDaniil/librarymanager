package com.library;

import org.jetbrains.annotations.Contract;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

public class TemplateGroup {
    private final BookTemplate template;
    private Map<UUID, Book> books;

    private TemplateGroup(BookTemplate template, Map<UUID, Book> books) {
        this.template = template;
        this.books = books;
    }

    public TemplateGroup(BookTemplate template) {
        this(template, new HashMap<>());
    }

    public BookTemplate getTemplate() {
        return template;
    }

    public Map<UUID, Book> getBooks() {
        return books;
    }

    public int bookCount () {
        return books.size();
    }

    public boolean addBook(Book book) {
        if (!template.checkBook(book)) {
            return false;
        } else {
            books.put(book.getId(),book);
            book.setGroup(this);
            return true;
        }
    }

    public void deleteBook (Book book) {
        if (book.getCurrentHandler()==null) {
            book.setGroup(null);
            books.remove(book.getId(),book);
        }
    }

    @Contract(value = "null -> false", pure = true)
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TemplateGroup)) return false;
        TemplateGroup that = (TemplateGroup) o;
        return Objects.equals(getTemplate(), that.getTemplate()) &&
                Objects.equals(getBooks(), that.getBooks());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTemplate(), getBooks());
    }
}
