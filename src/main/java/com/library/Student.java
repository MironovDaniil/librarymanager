package com.library;

import org.jetbrains.annotations.Contract;

import java.util.*;

public class Student {
    private final UUID id;
    private int grade;
    private final String name;
    private List<Book> currentBooksOnHands;

    public Student(UUID id, int grade, String name, List<Book> currentBooksOnHands) {
        this.id = id;
        this.grade = grade;
        this.name = name;
        this.currentBooksOnHands = currentBooksOnHands;
    }

    public Student(int grade, String name) {
        this(UUID.randomUUID(), grade, name, new ArrayList<>());
    }

    public UUID getId() {
        return id;
    }

    public int getGrade() {
        return grade;
    }

    public String getName() {
        return name;
    }

    public List<Book> getCurrentBooksOnHands() {
        return currentBooksOnHands;
    }

    void addBook(Book book) {
        currentBooksOnHands.add(book);
    }

    void removeBook(Book book) {
        currentBooksOnHands.remove(book);
    }

    public void graduate() {
        grade++;
    }

    public boolean finishedSchool() {
        return grade > 11;
    }

    public Map<String, Object> packToMap() {
        Map<String, Object> result = new HashMap<>(3);
        result.put("id",id.toString());
        result.put("name",name);
        result.put("grade",grade);
        if (currentBooksOnHands.size()==0) {
            result.put("currentBooksOnHands",null);
        } else {
            List<String> booksId = new ArrayList<>(currentBooksOnHands.size());
            for (Book book:currentBooksOnHands
                 ) {
                booksId.add(book.getId().toString());
            }
            result.put("currentBooksOnHands",booksId);
        }
        return result;
    }

    public void show() {
        System.out.println(id);
        System.out.println(grade);
        System.out.println(name);
    }

    @Contract(value = "null -> false", pure = true)
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Student)) return false;
        Student student = (Student) o;
        return getGrade() == student.getGrade() &&
                Objects.equals(getId(), student.getId()) &&
                Objects.equals(getName(), student.getName()) &&
                Objects.equals(getCurrentBooksOnHands(), student.getCurrentBooksOnHands());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getGrade(), getName(), getCurrentBooksOnHands());
    }
}
