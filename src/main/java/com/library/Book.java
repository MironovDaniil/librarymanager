package com.library;


import org.jetbrains.annotations.Contract;

import java.util.*;

public class Book {
    private final UUID id;
    private final int grade;
    private final int pageCount;
    private final int publishYear;
    private final String title;
    private final String authorName;
    private final String publishingHouse;
    private final String subject;
    private Student currentHandler;
    private Date timeGiven;
    private TemplateGroup group;

    public Book(){
        this(UUID.randomUUID(),0,0,0,"","","","",null,new Date(10),null);
    }

    public Book(UUID id, int grade, int pageCount, int publishYear, String title, String authorName,
                String publishingHouse, String subject, Student currentHandler, Date timeGiven, TemplateGroup group) {
        this.id = id;
        this.grade = grade;
        this.pageCount = pageCount;
        this.publishYear = publishYear;
        this.title = title;
        this.authorName = authorName;
        this.publishingHouse = publishingHouse;
        this.subject = subject;
        this.currentHandler = currentHandler;
        this.timeGiven = timeGiven;
        this.group = group;
    }

    public Book(int grade, int pageCount, int publishYear, String title, String authorName, String publishingHouse,
                String subject, TemplateGroup group) {
        this(UUID.randomUUID(), grade, pageCount, publishYear, title, authorName, publishingHouse, subject,
                null, new Date(10), group);
    }

    public UUID getId() {
        return id;
    }

    public int getGrade() {
        return grade;
    }

    public int getPageCount() {
        return pageCount;
    }

    public int getPublishYear() {
        return publishYear;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthorName() {
        return authorName;
    }

    public String getPublishingHouse() {
        return publishingHouse;
    }

    public String getSubject() {
        return subject;
    }

    public TemplateGroup getGroup() {
        return group;
    }

    public Student getCurrentHandler() {
        return currentHandler;
    }

    public Date getTimeGiven() {
        return timeGiven;
    }

    public void setGroup(TemplateGroup group) {
        this.group = group;
    }

    public void giveTo(Student student, Date date) {
        if (currentHandler != null) {
            System.out.println("Cannot give this book, student has it!");
            return;
        }
        currentHandler = student;
        timeGiven = date;
        student.addBook(this);
    }

    public void giveTo(Student student) {
        giveTo(student,new Date());
    }

    public void returnToLibrary() {
        currentHandler.removeBook(this);
        currentHandler = null;
        timeGiven = new Date(10);
    }

    public void show() {
        System.out.println(id);
        System.out.println(grade);
        System.out.println(pageCount);
        System.out.println(publishYear);
        System.out.println(title);
        System.out.println(authorName);
        System.out.println(publishingHouse);
        System.out.println(subject);
        if (currentHandler == null) {
            System.out.println("Book is in library");
        } else {
            System.out.println(currentHandler.getId());
        }
        System.out.println(Library.DATE_FORMAT.format(timeGiven));
    }

    public Map<String, Object> packToMap() {
        Map<String, Object> result = new HashMap<>(10);
        result.put("id", id.toString());
        result.put("grade", grade);
        result.put("pageCount", pageCount);
        result.put("publishYear", publishYear);
        result.put("title", title);
        result.put("authorName", authorName);
        result.put("publishingHouse", publishingHouse);
        result.put("subject", subject);
        if (currentHandler == null) {
            result.put("currentHandler", null);
        } else {
            result.put("currentHandler", currentHandler.getId().toString());
        }
        result.put("timeGiven", Library.DATE_FORMAT.format(timeGiven));
        return result;
    }

    @Contract(value = "null -> false", pure = true)
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Book)) return false;
        Book book = (Book) o;
        return getGrade() == book.getGrade() &&
                getPageCount() == book.getPageCount() &&
                getPublishYear() == book.getPublishYear() &&
                Objects.equals(getId(), book.getId()) &&
                Objects.equals(getTitle(), book.getTitle()) &&
                Objects.equals(getAuthorName(), book.getAuthorName()) &&
                Objects.equals(getPublishingHouse(), book.getPublishingHouse()) &&
                Objects.equals(getSubject(), book.getSubject()) &&
                Objects.equals(getCurrentHandler(), book.getCurrentHandler()) &&
                Objects.equals(getGroup(), book.getGroup());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getGrade(), getPageCount(), getPublishYear(), getTitle(), getAuthorName(), getPublishingHouse(), getSubject(), getCurrentHandler(), getGroup());
    }
}
