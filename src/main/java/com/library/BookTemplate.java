package com.library;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class BookTemplate {
    private final int grade;
    private final String title;
    private final String authorName;
    private final String subject;

    public BookTemplate(int grade, String title, String authorName, String subject) {
        this.grade = grade;
        this.title = title;
        this.authorName = authorName;
        this.subject = subject;
    }

    public BookTemplate(@NotNull Book book) {
        this.grade = book.getGrade();
        this.title = book.getTitle();
        this.authorName = book.getAuthorName();
        this.subject = book.getSubject();
    }

    public int getGrade() {
        return grade;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthorName() {
        return authorName;
    }

    public String getSubject() {
        return subject;
    }

    public boolean checkBook(Book book) {
        return (book.getAuthorName().equals(authorName) && book.getGrade() == grade
                && book.getTitle().equals(title) && book.getSubject().equals(subject));
    }

    public Map<String, Object> packToMap() {
        Map<String,Object> result = new HashMap<>(4);
        result.put("title",title);
        result.put("authorName",authorName);
        result.put("grade",grade);
        result.put("subject",subject);
        return result;
    }

    public void show() {
        System.out.println(grade);
        System.out.println(title);
        System.out.println(authorName);
        System.out.println(subject);
    }

    @Contract(value = "null -> false", pure = true)
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BookTemplate)) return false;
        BookTemplate that = (BookTemplate) o;
        return getGrade() == that.getGrade() &&
                Objects.equals(getTitle(), that.getTitle()) &&
                Objects.equals(getAuthorName(), that.getAuthorName()) &&
                Objects.equals(getSubject(), that.getSubject());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getGrade(), getTitle(), getAuthorName(), getSubject());
    }
}
