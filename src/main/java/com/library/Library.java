package com.library;

import java.text.SimpleDateFormat;
import java.util.*;

public class Library {

    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy");

    private final String name;
    private Map<UUID, Book> booksPerId;
    private Map<BookTemplate, TemplateGroup> groupsPerTemplate;
    private Map<UUID, Student> students;

    private Library(String name, Map<UUID, Book> booksPerId, Map<BookTemplate,
            TemplateGroup> groupsPerTemplate, Map<UUID, Student> students) {
        this.name = name;
        this.booksPerId = booksPerId;
        this.groupsPerTemplate = groupsPerTemplate;
        this.students = students;
    }

    public Library(String name) {
        this(name, new HashMap<>(), new HashMap<>(), new HashMap<>());
    }

    public String getName() {
        return name;
    }

    public Map<UUID, Book> getBooksPerId() {
        return booksPerId;
    }

    public Map<BookTemplate, TemplateGroup> getGroupsPerTemplate() {
        return groupsPerTemplate;
    }

    public Map<UUID, Student> getStudents() {
        return students;
    }

    public List<Student> getStudentByNameAndGrade(String name, int grade) {       //Возвращает список студентов с искомым именем на искомой параллели
        List<Student> result = new ArrayList<>(1);
        for (Map.Entry<UUID, Student> e : students.entrySet()) {
            if (e.getValue().getName().equals(name) && e.getValue().getGrade() == grade) {
                result.add(e.getValue());
            }
        }
        return result;
    }

    public List<Book> addSameBooks(int grade, int pageCount, int publishYear,               //Возвращает список добавленных книг
                                   String title, String authorName, String publishingHouse,
                                   String subject, int duplicatesCount) {
        List<Book> addedBooks = new ArrayList<>(duplicatesCount);
        for (int i = 0; i < duplicatesCount; i++) {
            addedBooks.add(new Book(grade, pageCount, publishYear, title, authorName,
                    publishingHouse, subject, null));
        }
        for (BookTemplate temp : groupsPerTemplate.keySet()) {
            if (temp.checkBook(addedBooks.get(0))) {
                TemplateGroup group = groupsPerTemplate.get(temp);
                for (Book book : addedBooks
                ) {
                    boolean wasAdded = group.addBook(book);
                    if (!wasAdded) {
                        System.out.println("Something is wrong! Book is adding not in it's group");
                    }
                    booksPerId.put(book.getId(), book);

                }
                return addedBooks;
            }
        }
        TemplateGroup group = new TemplateGroup(new BookTemplate(addedBooks.get(0)));
        for (Book book : addedBooks) {
            boolean wasAdded = group.addBook(book);
            if (!wasAdded) {
                System.out.println("Something is wrong! Book is adding not in it's group");
            }
            booksPerId.put(book.getId(), book);

        }
        groupsPerTemplate.put(group.getTemplate(), group);
        return addedBooks;
    }

    public void addExistingBook(Book book) {
        BookTemplate template = new BookTemplate(book);
        if (groupsPerTemplate.containsKey(template)) {
            TemplateGroup group = groupsPerTemplate.get(template);
            boolean wasAdded = group.addBook(book);
            if (!wasAdded) {
                System.out.println("Something is wrong! Book is adding not in it's group");
            }
            booksPerId.put(book.getId(), book);
        } else {
            TemplateGroup group = new TemplateGroup(template);
            groupsPerTemplate.put(group.getTemplate(), group);
            boolean wasAdded = group.addBook(book);
            if (!wasAdded) {
                System.out.println("Something is wrong! Book is adding not in it's group");
            }
            booksPerId.put(book.getId(), book);
        }
    }

    public Student addStudent (int grade,String name) {
        Student student = new Student(grade,name);
        students.put(student.getId(),student);
        return student;
    }

    public void addExistingStudent (Student student) {
        students.put(student.getId(),student);
    }

    public Book getBookById(UUID id) {
        if (booksPerId.get(id) == null) {
            System.out.println("No such book in the system!");
        }
        return booksPerId.get(id);
    }

    public TemplateGroup getGroupByTemplate(BookTemplate template) {
        return groupsPerTemplate.get(template);
    }

    public Student getStudentById(UUID id) {
        if (students.get(id) == null) {
            System.out.println("No such student in the system!");
        }
        return students.get(id);
    }

    public boolean deleteBook(Book book) {
        if (book == null) {
            System.out.println("No such book in the system!");
            return false;
        }
        if (book.getCurrentHandler() == null) {
            booksPerId.remove(book.getId(), book);
            TemplateGroup group = book.getGroup();
            group.deleteBook(book);
            return true;
        } else {
            System.out.println("Student has that book, can not delete!");
            return false;
        }
    }

    public boolean deleteStudent (Student student) {
        if (student.getCurrentBooksOnHands().size()==0) {
            students.remove(student.getId(),student);
            return true;
        } else {
            System.out.println("Can not delete this student, he has some books!");
            return false;
        }
    }

    public String returnBookToLibrary(Book book) {                       //Возвращает дату выдачи книги на руки
        Date timeAtWasGiven = book.getTimeGiven();
        book.returnToLibrary();
        return DATE_FORMAT.format(timeAtWasGiven);
    }

    public List<Book> deleteGroupByTemplate(BookTemplate template) {     //Возвращает список книг на руках, которые не были списаны
        List<Book> result = new ArrayList<>();
        List<Book> toDelete = new ArrayList<>();
        TemplateGroup group = groupsPerTemplate.get(template);
        for (Map.Entry<UUID, Book> bookEntry : group.getBooks().entrySet()) {
            toDelete.add(bookEntry.getValue());
        }
        for (Book b : toDelete) {
            if (!deleteBook(b)) {
                result.add(b);
            }
        }
        if (result.isEmpty()) {
            return null;
        } else {
            return result;
        }
    }

    public int countComplects(List<BookTemplate> templates) {
        int result = Integer.MAX_VALUE;
        for (BookTemplate t : templates) {
            TemplateGroup group = groupsPerTemplate.get(t);
            if (group.bookCount() < result) {
                result = group.bookCount();
            }
        }
        return result;
    }

    public int countStudentsOnParallel(int grade) {
        int result = 0;
        for (Map.Entry<UUID, Student> e : students.entrySet()) {
            if (e.getValue().getGrade() == grade) {
                result++;
            }
        }
        return result;
    }

    public int countBooksPerYear(int year) {
        int result = 0;
        for (Map.Entry<UUID, Book> e : booksPerId.entrySet()) {
            if (e.getValue().getPublishYear() == year) {
                result++;
            }
        }
        return result;
    }

    public int countTextbooksPerGrade(int grade) {
        int result = 0;
        for (Map.Entry<UUID, Book> e : booksPerId.entrySet()) {
            if (e.getValue().getGrade() == grade) {
                result++;
            }
        }
        return result;
    }


}
