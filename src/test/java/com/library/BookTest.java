package com.library;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

public class BookTest {
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy");
    private Book book;

    @Before
    public void setUp() {
        book = new Book(5,240,2013,
                "История древнего мира","Андреевская К.В.",
                "Мир","История",null);
    }

    @Test
    public void giveTo_and_returnToLibrary_shouldPrintTodayDateAndCantGiveBook () {
        Student student = new Student(5,"Вася Иванов");
        Student student2 = new Student(5,"Петя Павлов");
        book.giveTo(student);
        assertNotNull(book.getCurrentHandler());
        assertEquals(book,student.getCurrentBooksOnHands().get(0));
        System.out.println(DATE_FORMAT.format(book.getTimeGiven()));
        book.returnToLibrary();
        assertNull(book.getCurrentHandler());
        assertEquals(0,student.getCurrentBooksOnHands().size());
        book.giveTo(student);
        book.giveTo(student2);
        assertEquals(book,student.getCurrentBooksOnHands().get(0));
        assertEquals(0,student2.getCurrentBooksOnHands().size());

    }

}
