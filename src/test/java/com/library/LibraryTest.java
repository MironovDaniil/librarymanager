package com.library;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

public class LibraryTest {
    private Library library;

    @Before
    public void setUp() {
        library = new Library("Библиотека школы 291");
    }

    @Test
    public void addSameBooks_addingDifferentBooks_shouldGroupAsPlanned_shouldPrintTwoBookId() {
        library.addSameBooks(5, 200, 2010,
                "Математика+", "Васильев А.А.", "Европа",
                "Математика", 5);
        library.addSameBooks(4, 220, 2015,
                "Физика-", "Огурцов В.И.", "Азия",
                "Физика", 3);
        List<Book> added = library.addSameBooks(5, 200, 2010,
                "Математика+", "Васильев А.А.", "Европа",
                "Математика", 2);
        for (Book b:added
             ) {
            System.out.println(b.getId());
        }
        assertEquals(2, library.getGroupsPerTemplate().size());
        assertEquals(10, library.getBooksPerId().size());
        assertEquals(7, library.getGroupsPerTemplate().get(new BookTemplate
                (5, "Математика+", "Васильев А.А.", "Математика")).bookCount());
    }

    @Test
    public void deleteBook_checksDeletingAndNoDeletingWhenShould_shouldPrintCantDelete() {
        Student student = new Student(5, "Вася Иванов");
        library.addSameBooks(5, 200, 2010,
                "Математика+", "Васильев А.А.", "Европа",
                "Математика", 5);
        library.addSameBooks(4, 220, 2015,
                "Физика-", "Огурцов В.И.", "Азия",
                "Физика", 3);
        library.addSameBooks(5, 200, 2010,
                "Математика+", "Васильев А.А.", "Европа",
                "Математика", 2);
        library.addSameBooks(8, 500, 2008,
                "Химия для всех", "Шмекова И.Д.", "Америка",
                "Химия", 7);
        UUID id = UUID.randomUUID();
        for (Book book : library.getBooksPerId().values()
        ) {
            id = book.getId();
        }
        library.deleteBook(library.getBookById(id));
        assertEquals(16, library.getBooksPerId().size());
        for (Book book : library.getBooksPerId().values()
        ) {
            id = book.getId();
        }
        Book book = library.getBookById(id);
        book.giveTo(student);
        library.deleteBook(book);
        assertEquals(16, library.getBooksPerId().size());
    }

    @Test
    public void deleteGroupByTemplate_deleteGroup_getBooksCantDelete_shouldPrintCantDelete() {
        Student student = new Student(5, "Вася Иванов");
        library.addSameBooks(5, 200, 2010,
                "Математика+", "Васильев А.А.", "Европа",
                "Математика", 5);
        library.addSameBooks(4, 220, 2015,
                "Физика-", "Огурцов В.И.", "Азия",
                "Физика", 3);
        library.addSameBooks(5, 200, 2010,
                "Математика+", "Васильев А.А.", "Европа",
                "Математика", 2);
        library.addSameBooks(8, 500, 2008,
                "Химия для всех", "Шмекова И.Д.", "Америка",
                "Химия", 7);
        UUID id = UUID.randomUUID();
        for (Book b : library.getBooksPerId().values()
        ) {
            id = b.getId();
        }
        Book book = library.getBookById(id);
        book.giveTo(student);
        BookTemplate template = new BookTemplate(book);
        List<Book> onHands = library.deleteGroupByTemplate(template);
        assertEquals(book,onHands.get(0));
    }
}
