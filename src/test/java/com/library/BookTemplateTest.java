package com.library;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

public class BookTemplateTest {
    private BookTemplate template;

    @Before
    public void setUp() {
        template = new BookTemplate(5,"История древнего мира",
                "Андреевская К.В.","История");
    }

    @Test
    public void checkBook_shouldSucceedBoth() {
        TemplateGroup templateGroup1 =  Mockito.mock(TemplateGroup.class);
        Book book1 = new Book(5,240,2013,
                "История древнего мира","Андреевская К.В.",
                "Мир","История",templateGroup1);
        Book book2 = new Book(5,240,2014,
                "История древнего мира","Андреевская К.В.",
                "Мир","История",templateGroup1);
        Book book3 = new Book(5,240,2014,
                "История нового мира","Андреевская К.В.",
                "Мир","История",templateGroup1);
        assertTrue(template.checkBook(book1));
        assertTrue(template.checkBook(book2));
        assertFalse(template.checkBook(book3));
    }

}
